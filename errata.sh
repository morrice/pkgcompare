#!/bin/bash
RELEASE=9
REPOSITORIES="BaseOS AppStream"
EXCLUDED_PACKAGES="kpatch redhat-release redhat-indexhtml redhat-cloud-client-configuration"
COLOUR_NORMAL="\\033[0;39m"
COLOUR_RED="\\033[1;31m"
COLOUR_BLUE="\\033[1;34m"
ALMACOUNT=0
ROCKYCOUNT=0
NOBODYCOUNT=0

usage() {
cat << EOF
usage: $0 -r <8,9> -d <##> -b -g

This script provides an insight into the speed of which Rocky and Alma release CVE fixes against RHEL

OPTIONS:

  -b        include bugfix packages (default: no)
  -r        release (default: 9)
  -d        Only include CVEs from this time (days)
  -x        Exclude 'NotAvailable' packages from the tally (a real fight?)
  -h        display this help
  -g        generate graph
  -a        display advisories instead of dates in the graph
EOF
exit
}

while getopts "axbgr:d:h" option
do
  case $option in
    a)
      GRAPH_ADVISORY=1
      ;;
    x)
      EXCLUDE=1
      ;;
    g)
      GRAPH=1
      ;;
    b)
      BUGS=1
      ;;
    h)
      usage
      ;;
    r)
      RELEASE=$OPTARG
      ;;
    d)
      # no error checking here
      DAYS=$(date +%s --date="$OPTARG days ago" | cut -d. -f1)
      ;;
  esac
done

RHEL_FP=/mnt/data1/dist/cdn.redhat.com/content/dist/rhel${RELEASE}/${RELEASE}/x86_64/
ROCKY_FP=/mnt/data1/dist/rockylinux/${RELEASE}/
ALMA_FP=/mnt/data1/dist/almalinux/${RELEASE}/

format_rhel_repo () {
  if [[ $1 == "PowerTools" ]]; then
    RHEL_FP_FORMATTED=${2}codeready-builder/
  else
    RHEL_FP_FORMATTED=${2}${1,,}/
  fi
}

PACKAGE_DETAILS=()
for REPO in $REPOSITORIES; do
  format_rhel_repo $REPO $RHEL_FP
  echo "Crunching data for $REPO [${RELEASE} release]"
  UPDATEINFO=$(find $RHEL_FP_FORMATTED/os/ -name '*updateinfo.xml*')
  if [ $BUGS ]; then
    INTERESTING_PACKAGES=$(zegrep "<severity>|<id>|<issued date|<filename>" $UPDATEINFO |egrep "<severity>" -A1 -B2 | cut -d\" -f2 | cut -d\> -f2 | cut -d\< -f1 | sed 's/--/\n/g' | sed 's/ /_/g' | sed '/^\s*$/d' | paste - - - - -d, | sort)
  else
    INTERESTING_PACKAGES=$(zegrep "<severity>|<id>|<issued date|<filename>" $UPDATEINFO |egrep "<severity>Critical|<severity>Moderate|<severity>Important" -A1 -B2 | cut -d\" -f2 | cut -d\> -f2 | cut -d\< -f1 | sed 's/--/\n/g' | sed 's/ /_/g' | sed '/^\s*$/d' | paste - - - - -d, | sort)
  fi
  for INTERESTING_PACKAGE in $INTERESTING_PACKAGES; do
    RHEL_PACKAGE=$(echo $INTERESTING_PACKAGE | cut -d, -f4)
    RHEL_SEVERITY=$(echo $INTERESTING_PACKAGE | cut -d, -f3)
    RHEL_ID=$(echo $INTERESTING_PACKAGE | cut -d, -f1)
    RHEL_EPOCH=$(date +%s --date="$(echo $INTERESTING_PACKAGE | cut -d, -f2 | sed 's/_/ /g')")
    if [ $DAYS ]; then
      if [ $DAYS -gt $RHEL_EPOCH ]; then
        continue
      fi
    fi
    ROCKY_EPOCH=$(find $ROCKY_FP/${REPO}/ -name $RHEL_PACKAGE -printf '%T@' 2>/dev/null | cut -d. -f1)
    ALMA_EPOCH=$(find $ALMA_FP/${REPO}/ -name $RHEL_PACKAGE -printf '%T@' 2>/dev/null | cut -d. -f1)
    if [[ $ROCKY_EPOCH == "" ]]; then
      # some rocky packages have the wrong disttag (el9 vs el9_0 from RHEL) :/
      STRIPPED_PACKAGE=$(echo ${RHEL_PACKAGE} | rev | cut -d. -f4- | rev)
      ROCKY_PACKAGE="${STRIPPED_PACKAGE}.*.rpm"
      ROCKY_EPOCH=$(find $ROCKY_FP/${REPO}/ -name $ROCKY_PACKAGE -printf '%T@' 2>/dev/null | cut -d. -f1)
      if [[ $ROCKY_EPOCH == "" ]]; then
        ROCKY_EPOCH="NotAvailable"
      fi
    fi
    if [[ $ALMA_EPOCH == "" ]]; then
      # some alma packages are suffixed with 'alma' (firefox, thunderbird, etc)
      STRIPPED_PACKAGE=$(echo ${RHEL_PACKAGE} | rev | cut -d. -f3- | rev)
      ALMA_PACKAGE="${STRIPPED_PACKAGE}.alma.*.rpm"
      ALMA_EPOCH=$(find $ALMA_FP/${REPO}/ -name $ALMA_PACKAGE -printf '%T@' 2>/dev/null | cut -d. -f1)
      if [[ $ALMA_EPOCH == "" ]]; then
        ALMA_EPOCH="NotAvailable"
      fi
    fi
      
    # Some RPMS exist in both BaseOS and AppStream, but the src rpm will only
    # exist in one (likely BaseOS). Exclude the item if no src rpm exists in $REPO
    if [[ $RHEL_EPOCH != "" ]]; then
      for EXCLUDED_PACKAGE in $EXCLUDED_PACKAGES; do
        if [[ "$RHEL_PACKAGE" == *"$EXCLUDED_PACKAGE"* ]]; then
          continue 2
        fi
      done
      PACKAGE_DETAILS+=("${RHEL_ID},${RHEL_SEVERITY},${RHEL_PACKAGE},${RHEL_EPOCH},${ROCKY_EPOCH},${ALMA_EPOCH}")
    fi
  done
done
rm -f {alma,rocky}.dat 2>/dev/null
for PACKAGE_DETAIL in ${PACKAGE_DETAILS[@]}; do
  ID=$(echo $PACKAGE_DETAIL | cut -d, -f1)
  SEVERITY=$(echo $PACKAGE_DETAIL | cut -d, -f2)
  PACKAGE=$(echo $PACKAGE_DETAIL | cut -d, -f3)
  RHEL_EPOCH=$(echo $PACKAGE_DETAIL | cut -d, -f4)
  ROCKY_EPOCH=$(echo $PACKAGE_DETAIL | cut -d, -f5)
  ALMA_EPOCH=$(echo $PACKAGE_DETAIL | cut -d, -f6)
  # skip records that do not have an epoch for now
  # maybe this should be tweaked
  if [[ "$ROCKY_EPOCH" != "NotAvailable" ]] && [[ "$ALMA_EPOCH" == "NotAvailable" ]]; then
    ROCKY_DELTA=$(echo "($ROCKY_EPOCH - $RHEL_EPOCH) / 60 / 60" | bc)
    if [ $ROCKY_DELTA -lt 0 ]; then
      continue
    fi
    if [ $EXCLUDE ]; then
      continue
    fi
    ALMA_DELTA="NotAvailable"
    WINNER="Rocky"
    WINNER_COLOUR=$COLOUR_RED
    let "ROCKYCOUNT+=1"
  elif [[ "$ALMA_EPOCH" != "NotAvailable" ]] && [[ "$ROCKY_EPOCH" == "NotAvailable" ]]; then
    ALMA_DELTA=$(echo "($ALMA_EPOCH - $RHEL_EPOCH) / 60 / 60" | bc)
    if [ $ALMA_DELTA -lt 0 ]; then
      continue
    fi
    if [ $EXCLUDE ]; then
      continue
    fi
    ROCKY_DELTA="NotAvailable"
    WINNER="Alma"
    WINNER_COLOUR=$COLOUR_BLUE
    let "ALMACOUNT+=1"
  elif [[ "$ALMA_EPOCH" == "NotAvailable" ]] && [[ "$ROCKY_EPOCH" == "NotAvailable" ]]; then
    echo "[$ID,$SEVERITY,$PACKAGE]: No winner :("
    let "NOBODYCOUNT+=1"
    continue
  else
    ROCKY_DELTA=$(echo "($ROCKY_EPOCH - $RHEL_EPOCH) / 60 / 60" | bc)
    ALMA_DELTA=$(echo "($ALMA_EPOCH - $RHEL_EPOCH) / 60 / 60" | bc)
    if [ $ALMA_DELTA -lt 0 ] || [ $ROCKY_DELTA -lt 0 ] ; then
      continue
    fi
    # write out data to files
    if [ $GRAPH_ADVISORY ]; then
      echo "$ID $ALMA_DELTA" >> alma.dat
      echo "$ID $ROCKY_DELTA" >> rocky.dat
    else
      echo "$RHEL_EPOCH $ALMA_DELTA" >> alma.dat
      echo "$RHEL_EPOCH $ROCKY_DELTA" >> rocky.dat
    fi
    CHECK_WINNER=$(echo "$ROCKY_DELTA - $ALMA_DELTA" | bc)
    if [ $CHECK_WINNER -gt 0 ]; then
      WINNER="Alma"
      WINNER_COLOUR=$COLOUR_BLUE
      let "ALMACOUNT+=1" 
    else
      WINNER="Rocky"
      WINNER_COLOUR=$COLOUR_RED
      let "ROCKYCOUNT+=1" 
    fi
  fi
  echo -n "[$ID,$SEVERITY,$PACKAGE]: Hours delay: Rocky $ROCKY_DELTA, Alma $ALMA_DELTA"
  echo -ne $WINNER_COLOUR " $WINNER"
  echo -e $COLOUR_NORMAL "wins!"
done
ALMA_AVERAGE=$(cat alma.dat | awk '{sum+=$2} END { print sum/NR}')
ALMA_TOTAL=$(cat alma.dat | awk '{sum+=$2} END { print sum}')
ROCKY_AVERAGE=$(cat rocky.dat | awk '{sum+=$2} END { print sum/NR}')
ROCKY_TOTAL=$(cat rocky.dat | awk '{sum+=$2} END { print sum}')
if [ $ALMACOUNT -gt $ROCKYCOUNT ]; then
  echo -en $COLOUR_BLUE "Alma wins"
  echo -e $COLOUR_NORMAL "[ $ALMACOUNT Alma wins ($ALMA_AVERAGE average hours, $ALMA_TOTAL total hours) versus $ROCKYCOUNT Rocky wins ($ROCKY_AVERAGE average hours, $ROCKY_TOTAL total hours) (and $NOBODYCOUNT packages that were not built by either) ]"
else
  echo -en $COLOUR_RED "Rocky wins"
  echo -e $COLOUR_NORMAL "[ $ROCKYCOUNT Rocky wins ($ROCKY_AVERAGE average hours, $ROCKY_TOTAL total hours) versus $ALMACOUNT Alma wins ($ALMA_AVERAGE average hours, $ALMA_TOTAL total hours) (and $NOBODYCOUNT packages that were not built by either)]"
fi
if [ $GRAPH ]; then
  echo "set title 'Delay of CVE updates'" > plot.gp
  echo "set ylabel 'Delay in hours'" >> plot.gp
  if [ $GRAPH_ADVISORY ]; then
    echo "set xlabel 'RHEL advisory'" >> plot.gp
    echo "set xtics rotate by -90" >> plot.gp
    echo 'set xtics font ", 5' >> plot.gp
  else
    echo "set xlabel 'Date'" >> plot.gp
    echo "set xdata time" >> plot.gp
    echo "set timefmt '%s'" >> plot.gp
    echo "set format x '%Y%m%d'" >> plot.gp
    echo "set xtics rotate by -45" >> plot.gp
  fi
  echo "set grid" >> plot.gp
  echo "set autoscale" >> plot.gp
  if [ $GRAPH_ADVISORY ]; then
    echo "plot 'rocky.dat' using 2:xtic(1) lt rgb 'red' w l title 'rocky', 'alma.dat' using 2:xtic(1) lt rgb 'blue' w l title 'alma'" >> plot.gp
  else
    echo "plot 'rocky.dat' using 1:2 lt rgb 'red' w l title 'rocky', 'alma.dat' using 1:2 lt rgb 'blue' w l title 'alma'" >> plot.gp
  fi
  echo "Graphs generated, you can now run: gnuplot -p plot.gp"
  sort -o alma.dat alma.dat
  sort -o rocky.dat rocky.dat
fi
