#!/bin/bash

usage() {
cat << EOF
usage: $0 -d <all,almalinux,centos,rockylinux> -m -v -x

This script provides an insight into the package differences between alma,centos and rocky compared to RHEL upstream

OPTIONS:

  -d        distro to work on. use 'all' or comma separated list for multiple (almalinux,centos,rockylinux)
  -r        repositories to work on (default: BaseOS,AppStream). comma separated for multiple)
  -m        do not include missing packages (default: false)
  -v        do not include package mismatches (default: false)
  -w        do not include packages that exist in the wrong repo (default: false)
  -x        run with extra debug information
  -h        display this help
EOF
exit
}

format_rhel_repo () {
  if [[ $1 == "PowerTools" ]]; then
    RHELREPO=${2}codeready-builder/os
  else
    RHELREPO=${2}${1,,}/os
  fi
}

EXCLUDE_MISSING=0
EXCLUDE_MISMATCH=0
EXCLUDE_WRONG=0
DEBUG=0

while getopts "d:r:mvwxh" option
do
  case $option in
    h)
      usage
      ;;
    x)
      DEBUG=1
      ;;
    m)
      EXCLUDE_MISSING=1
      ;;
    v)
      EXCLUDE_MISMATCH=1
      ;;
    w)
      EXCLUDE_WRONG=1
      ;;
    d)
      DISTRIBUTIONS=`echo $OPTARG | sed 's/,/ /g'`
      ;;
    r)
      REPOSITORIES=`echo $OPTARG | sed 's/,/ /g'`
      ;;
  esac
done

if [ $# -eq 0 ]; then
  usage
fi

if [[ -z $DISTRIBUTIONS ]] || [[ "$DISTRIBUTIONS" == "all" ]]; then
  DISTRIBUTIONS="almalinux rockylinux centos"
fi

if [[ -z $REPOSITORIES ]]; then
  REPOSITORIES="BaseOS AppStream"
fi

RHEL_URL=/mnt/data1/dist/cdn.redhat.com/content/dist/rhel8/8/x86_64/
# no point checking aarch64 as this is not available on rocky,alma yet
ARCHLIST="x86_64,noarch"
# RHEL packages that will never be rebuilt by clones, either because they
# are RHEL/RedHat specific or because they are subpackages that are no longer
# built with the latest version of the source package
EXCLUDELIST="insights-client|rhc|rhsm-gtk|rhsm-icons|spice-client-win-x64|spice-client-win-x86|spice-qxl-wddm-dod|spice-vdagent-win-x64|spice-vdagent-win-x86|virt-who|virtio-win|subscription-manager|kmod-redhat|kpatch|redhat-|dnf-utils|libstoragemgmt-netapp-plugin|samba-dc-libs"
TMPDIRECTORY=`mktemp -d`
for REPO in $REPOSITORIES
do
  for DIST in $DISTRIBUTIONS
  do
    if [ ! -d /mnt/data1/dist/${DIST}/8/${REPO}/x86_64/os ]; then
      echo "Error: /mnt/data1/dist/${DIST}/8/${REPO}/x86_64/os does not exist, exiting ..."
      exit
    fi
    repoquery --repofrompath=randomid,/mnt/data1/dist/${DIST}/8/${REPO}/x86_64/os --disablerepo=* --enablerepo=randomid -qa --qf="%{name}:%{version}:%{release}:%{arch}" --archlist=$ARCHLIST > $TMPDIRECTORY/$DIST.$REPO
  done
  format_rhel_repo $REPO $RHEL_URL
  repoquery --repofrompath=randomid,${RHELREPO} --disablerepo=* --enablerepo=randomid -qa --qf="%{name}:%{version}:%{release}:%{arch}" --archlist=$ARCHLIST | egrep -v "$EXCLUDELIST" > $TMPDIRECTORY/RHEL.$REPO
done

for REPO in $REPOSITORIES
do
  ALMA_MISSING=0
  ALMA_MISMATCH=0
  ALMA_WRONGREPO=0
  CENTOS_MISSING=0
  CENTOS_MISMATCH=0
  CENTOS_WRONGREPO=0
  ROCKY_MISSING=0
  ROCKY_MISMATCH=0
  ROCKY_WRONGREPO=0
  for LINE in `cat $TMPDIRECTORY/RHEL.$REPO`
  do
    PACKAGE=`echo $LINE | cut -d: -f1`
    ARCH=`echo $LINE | cut -d: -f4`
    VERSION=`echo $LINE | cut -d: -f2`
    RELEASE=`echo $LINE | cut -d: -f3 | sed 's/.el8.*//'`

    for DIST in $DISTRIBUTIONS
    do
      grep -q "^${PACKAGE}:.*${ARCH}" $TMPDIRECTORY/$DIST.$REPO
      if [ $? -ne 0 ]; then
        IS_WRONG=0
        # check for BaseOS/AppStream mismatches
        if [ $EXCLUDE_WRONG -eq 0 ]; then
          if [ -f $TMPDIRECTORY/$DIST.AppStream ] && [ -f $TMPDIRECTORY/$DIST.BaseOS ]; then
            if [ "$REPO" == "BaseOS" ]; then
              OTHERREPO="AppStream"
            else
              OTHERREPO="BaseOS"
            fi
            grep -q "^${PACKAGE}:.*${ARCH}" $TMPDIRECTORY/RHEL.$OTHERREPO
            # ignore if the package exists in both RHEL AppStream/BaseOS
            if [ $? -eq 1 ]; then
              grep -q "^${PACKAGE}:.*${ARCH}" $TMPDIRECTORY/$DIST.$OTHERREPO
              if [ $? -eq 0 ]; then
                IS_WRONG=1
                if [ $DEBUG -eq 1 ]; then
		  echo "${PACKAGE}:${ARCH} exists in $OTHERREPO, instead of $REPO ($DIST)"
                fi
                case $DIST in
                  almalinux)
                    let "ALMA_WRONGREPO+=1"
                    ;;
                  centos)
                    let "CENTOS_WRONGREPO+=1"
                    ;;
                  rockylinux)
                    let "ROCKY_WRONGREPO+=1"
                    ;;
                esac
              fi
            fi
          fi
        fi
        if [ $EXCLUDE_MISSING -eq 0 ] && [ $IS_WRONG -eq 0 ]; then
          IS_MISSING=0
          format_rhel_repo $REPO $RHEL_URL
          CHECK_OBSOLETES=`repoquery --repofrompath=randomid,${RHELREPO} --disablerepo=* --enablerepo=randomid --archlist=$ARCHLIST --whatobsoletes $PACKAGE |wc -l`
          if [ $CHECK_OBSOLETES -lt 1 ]; then
            IS_MISSING=1
            if [ $DEBUG -eq 1 ]; then
              echo "${PACKAGE}:${ARCH} does not exist in $DIST ($REPO)"
            fi
            case $DIST in
              almalinux)
                let "ALMA_MISSING+=1"
                ;;
              centos)
                let "CENTOS_MISSING+=1"
                ;;
              rockylinux)
                let "ROCKY_MISSING+=1"
                ;;
            esac
          else
            # it's not really, but for the version mismatch if we need to set this
            IS_MISSING=1
            if [ $DEBUG -eq 1 ]; then
              echo "${PACKAGE}:${ARCH} is an obsoleted package in RHEL, ignoring"
            fi
          fi
        fi
      # version mismatch code
      if [ $EXCLUDE_MISMATCH -eq 0 ] && [ $IS_MISSING -eq 0 ]; then
        if [[ $RELEASE == *"module"* ]]; then
          RHEL_VERSION="${PACKAGE}:${VERSION}:${ARCH}"
          DIST_VERSION=`grep "^${PACKAGE}:.*${ARCH}" $TMPDIRECTORY/$DIST.$REPO | cut -d: -f1,2,4`
        else
          RHEL_VERSION="${PACKAGE}:${VERSION}:${RELEASE}:${ARCH}"
          TMP_DIST_VERSION=`grep "^${PACKAGE}:.*${ARCH}" $TMPDIRECTORY/$DIST.$REPO`
          if [[ $TMP_DIST_VERSION == *".el8"* ]]; then
            DIST_VERSION="`grep "^${PACKAGE}:.*${ARCH}" $TMPDIRECTORY/$DIST.$REPO | sed 's/.el8.*//'`:${ARCH}"
          else
            DIST_VERSION=$TMP_DIST_VERSION
          fi
        fi
        if [ "$RHEL_VERSION" != "$DIST_VERSION" ]; then
          if [ $EXCLUDE_MISMATCH -eq 0 ]; then
            if [ $DEBUG -eq 1 ]; then
              echo "RHEL $RHEL_VERSION is a mismatch to $DIST $DIST_VERSION"
            fi
            case $DIST in
              almalinux)
                let "ALMA_MISMATCH+=1"
                ;;
              centos)
                let "CENTOS_MISMATCH+=1"
                ;;
              rockylinux)
                let "ROCKY_MISMATCH+=1"
                ;;
            esac
          fi
        fi
      fi
    fi
    done
  done
  RHEL_TOTAL=`cat $TMPDIRECTORY/RHEL.$REPO |wc -l`
  echo "RHEL-${REPO} packages total: $RHEL_TOTAL"
  if [ $DEBUG -eq 1 ]; then
    echo "RedHat tmp file: $TMPDIRECTORY/RHEL.$REPO"
  fi
  if [[ $DISTRIBUTIONS == *"alma"* ]]; then
    echo "AlmaLinux: missing [$ALMA_MISSING], version mismatches [$ALMA_MISMATCH], wrong repo [$ALMA_WRONGREPO]"
    if [ $DEBUG -eq 1 ]; then
      echo "Alma tmp file: $TMPDIRECTORY/ALMA.$REPO"
    fi
  fi
  if [[ $DISTRIBUTIONS == *"centos"* ]]; then
    echo "CentOS Linux: missing [$CENTOS_MISSING], version mismatches [$CENTOS_MISMATCH], wrong repo [$CENTOS_WRONGREPO]"
    if [ $DEBUG -eq 1 ]; then
      echo "Centos tmp file: $TMPDIRECTORY/CENTOS.$REPO"
    fi
  fi
  if [[ $DISTRIBUTIONS == *"rocky"* ]]; then
    echo "RockyLinux: missing [$ROCKY_MISSING], version mismatches [$ROCKY_MISMATCH], wrong repo [$ROCKY_WRONGREPO]"
    if [ $DEBUG -eq 1 ]; then
      echo "Rocky tmp file: $TMPDIRECTORY/ROCKY.$REPO"
    fi
  fi
done
